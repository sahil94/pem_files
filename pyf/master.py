import os
from subprocess import Popen
import subprocess
import numpy as np

look_for = "[BEGIN_NEURON, END_NEURON] = "
lf3 = "[BEGIN_NEURON, END_NEURON] =  [-1,4]"
look_in = "constants.py"
lf2 = " import BEGIN_NEURON, END_NEURON"
start = -1

with open(look_in, 'r') as mf:
    look_in = mf.read()
with open("turnoff.py", 'r') as mf:
    li2 = mf.read()

index = look_in.find(look_for) + len(look_for)
i2 = li2.find(lf2)
i3 = look_in.find(lf3) + len(lf3)

n2 = li2[:i2] + '_4' + li2[i2:]
new = look_in[:index] + '[252,256] ' + look_in[i3:]
print(new)

for end in range(4,257,4):
    new = look_in[:index] + '[%s,%s] '%(start,end) + look_in[i3:]
    newn = "constants_%d.py"%end
    with open(newn, 'w') as mf:
        mf.write(new)
    n2 = li2[:i2] + '_%d'%end + li2[i2:]
    n2n = "turnoff_%d.py"%end
    with open(n2n, 'w') as mf:
        mf.write(n2)
    s = 'nohup python -u turnoff_%d.py > output_turnoff_%d &'%(end, end)
    #t = 'nohup python -u a.py > %d.txt &'%end
    print(s)
    p = Popen([s],stdout=subprocess.PIPE,shell=True)
    start = end

