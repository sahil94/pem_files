# -*- coding: utf-8 -*-
import tensorflow as tf
import numpy as np
np.set_printoptions(suppress=True)
import random

from game_state import GameState
from plot_s import GameACLSTMNetwork
from a3c_training_thread import A3CTrainingThread
from rmsprop_applier import RMSPropApplier

from constants import ACTION_SIZE
from constants import PARALLEL_SIZE
from constants import CHECKPOINT_DIR
from constants import RMSP_EPSILON
from constants import RMSP_ALPHA
from constants import GRAD_NORM_CLIP
from constants import USE_GPU
from constants import USE_LSTM
from constants import SAMPLE_FROM_POLICY
from constants import DISPLAY
from constants import ROMS
from constants import LOCAL_T_MAX
from constants import BEGIN_NEURON, END_NEURON
from constants import BASELINES

NUM_GAMES = len(ROMS)

def do_all():
    print("I begin with %d and end with %d neuron"%(BEGIN_NEURON, END_NEURON))
    def div0( a, b ):
        """ ignore / 0, div0( [-1, 0, 1], 0 ) -> [0, 0, 0] """
        with np.errstate(divide='ignore', invalid='ignore'):
            c = np.true_divide( a, b )
            c[ ~ np.isfinite( c )] = 0  # -inf inf NaN
        return c

    def choose_action(pi_values, sample_from = SAMPLE_FROM_POLICY):
      if np.random.random() < sample_from:
        values = []
        sum = 0.0
        for rate in pi_values:
          sum = sum + rate
          value = sum
          values.append(value)

        r = random.random() * sum
        for i in range(len(values)):
          if values[i] >= r:
            return i;
        #fail safe
      else:
        return np.argmax(pi_values)
      return len(values)-1

    # use CPU for display tool
    device = "/cpu:0"
    if USE_LSTM:
      global_network = GameACLSTMNetwork(ACTION_SIZE, 0, device)

    sess = tf.Session()

    saver = tf.train.Saver()
    checkpoint = tf.train.get_checkpoint_state(CHECKPOINT_DIR)
    if checkpoint and checkpoint.model_checkpoint_path:
      saver.restore(sess, checkpoint.model_checkpoint_path)
      print("checkpoint loaded:", checkpoint.model_checkpoint_path)
    else:
      print("Could not find old checkpoint")
    game_states = []
    for i in xrange(NUM_GAMES):
        game_state = GameState(0, ROMS[i], display=DISPLAY)
        game_states.append(game_state)

    alp95 = 1.96
    on = 0.0*np.zeros(256)
    max_steps_episode = np.zeros(NUM_GAMES)

    avg_turnoff = np.zeros((NUM_GAMES,257))

    for off_index in xrange(BEGIN_NEURON,END_NEURON):
      mask_off = np.ones(256)
      if off_index != -1:
        mask_off[off_index] = 0
      for i in xrange(NUM_GAMES):
        game_state = game_states[i]
        average_episode_score = 0
        avgs = np.zeros(256)
        #on = np.zeros(256)
        episode_score = 0
        num_episodes = 0
        sum_sq_scores = 0.0
        steps = 0
        steps_fig = 0
        max_steps_episode[i] = 0
        prev_episode_score = 0.0
        while (num_episodes < 20):
          pi_values, lo = global_network.run_policy(sess, game_state.s_t, mask_off)
          action = choose_action(pi_values)
          fs = 4
          steps += 1
          steps_fig += 1 		#Steps in the episode
          avgs += lo
          for ii,l in enumerate(lo):
              if l >= 0.5 or l <= -0.5:
                  on[ii] += 1.0
          game_state.process(action, fs)
          game_state.update()
          episode_score += game_state.reward
          if(steps%1000 == 0):
            print('Bro Im not yet dead ::::::::::STEP NUMBER {}::::::::'.format(steps))
          if (game_state.terminal is True) or (steps_fig > 20000):
              if(steps_fig>max_steps_episode[i]):
                  max_steps_episode[i] = steps_fig
              steps_fig = 0
              average_episode_score += episode_score
              num_episodes += 1.0
              sum_sq_scores += episode_score**2
              avg = average_episode_score/float(num_episodes)
              std = np.sqrt(sum_sq_scores / num_episodes - avg ** 2)
              lower = avg - alp95*std/np.sqrt(num_episodes)
              upper = avg + alp95*std/np.sqrt(num_episodes)
              print("Game: %s Episode %d:: Score: %d Avg: %f Std: %f Conf Int: (%f, %f)"%
                      (ROMS[i],
                       num_episodes,
                       episode_score,
                       avg,
                       std,
                       lower,
                       upper))
              episode_score = 0
              global_network.reset_state()
              game_state.reset()
        avg_turnoff[i][off_index] = avg

    np.savetxt('turnoff_B' + str(BEGIN_NEURON) + '_E' + str(END_NEURON) + '.csv',avg_turnoff,delimiter = ',')


do_all()
